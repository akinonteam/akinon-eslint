module.exports = {
  extends: [
    'eslint:recommended',
    './rules/akinon-config',
  ].map(require.resolve),
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
};
